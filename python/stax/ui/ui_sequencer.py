# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

import bpy
import bl_ui
import bpy.utils.previews
from bpy.types import Header
from bpy.utils import register_class, unregister_class

from . import ui_drawing
from .ui_notes import NOTES_PT_pending_text_note, NOTES_PT_text_notes
from stax.utils.utils_core import get_displayed_tracks
from stax.utils.utils_timeline import get_media_sequence
from stax.utils.utils_ui import (
    draw_emphasize_drawing_slider,
    draw_set_preview_range,
    get_stax_icon,
)


class SEQUENCER_HT_header(Header):
    """Sequencer space header"""

    bl_space_type = "SEQUENCE_EDITOR"

    def draw(self, context):
        space_data = context.space_data
        user_preferences = context.scene.user_preferences

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        if context.scene is not bpy.data.scenes["Scene"]:
            return

        if user_preferences.show_blender_ui:
            # Draw original Blender UI
            bl_ui.space_sequencer.SEQUENCER_HT_header.draw(self, context)

        else:
            # Displayed in Sequencer
            if space_data.view_type == "SEQUENCER":
                self.draw_sequencer(context)

            elif space_data.view_type == "PREVIEW":  # Displayed in Preview
                self.draw_preview(context)

    def draw_sequencer(self, context):
        """Draw UI for Sequencer space"""
        layout = self.layout

        # Toggle annotations drawing keyframes editing
        layout.operator(
            "sequencer.toggle_annotations_keyframes",
            text="Annotations Keyframes Editing",
            icon="GP_MULTIFRAME_EDITING",
        )

        layout.separator_spacer()

        # Preview range
        draw_set_preview_range(context.scene, layout)

        layout.separator_spacer()

        row = layout.row(align=True)

        # Toggle versions
        if context.scene.versions_displayed_sequence:
            row.operator(
                "sequencer.toggle_versions",
                text="Versions",
                icon="SEQ_STRIP_META",
            )
        else:
            row.operator(
                "sequencer.toggle_versions",
                text="Versions",
                icon="SEQ_STRIP_DUPLICATE",
            )

        # View All Clips Button
        layout.separator()
        layout.operator("sequencer.view_all", text="View all clips")

    def draw_preview(self, context):
        """Draw UI for Preview space"""
        layout = self.layout

        scene = context.scene
        active_strip = scene.sequence_editor.active_strip

        # Abort/Publish Reviews button
        if context.scene.review_session_active:
            layout.separator()
            row = layout.row()
            row.operator("scene.abort_reviews", icon="CANCEL")
            row.operator("scene.publish_reviews", icon="CHECKMARK")

        layout.separator_spacer()

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        # Draw same tools as in Drawing workspace
        if scene is not bpy.data.scenes["Scene"]:
            ui_drawing.VIEW3D_HT_header.draw_stax_tools(self)
            layout.separator_spacer()
            return
        else:
            # Display media name in preview
            if active_strip:
                media_sequence = get_media_sequence(active_strip)
                is_reviewed = "*" if media_sequence.get("reviewed") else ""
                layout.label(text=f"{is_reviewed} {media_sequence.name}")

        layout.separator_spacer()

        # Current strip Fusion types and alpha
        layout.popover(panel="SEQUENCER_PT_compare", text="", icon="MOD_BEVEL")

        layout.separator()

        # Emphasize drawings value
        draw_emphasize_drawing_slider(layout)

        layout.separator()

        # Stax Sequencer view tools
        row = layout.row(align=True)
        row.operator("wm.splitview", text="", icon="UV_ISLANDSEL")
        row.operator("wm.detachview", text="", icon="WINDOW")


class SEQUENCER_HT_tool_header(Header):
    """Sequencer tool header"""

    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "TOOL_HEADER"

    def draw(self, context):
        space_data = context.space_data

        # Displayed in Sequencer
        if space_data.view_type == "SEQUENCER":
            self.draw_sequencer(context)

        elif space_data.view_type == "PREVIEW":  # Displayed in Preview
            self.draw_preview(context)

    def draw_sequencer(self, context):
        """Draw UI for Sequencer space"""
        layout = self.layout
        scene = context.scene

        # Reports banner for messages to the user
        layout.template_reports_banner()

        layout.separator_spacer()

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        # Don't display Main UI and inform the user
        if scene is not bpy.data.scenes["Scene"]:
            layout.label(
                text="You are currently in Advanced Drawing mode, please complete or cancel your drawing",
                icon="INFO",
            )
            layout.separator_spacer()
            return

        # Display status changer
        active_strip = bpy.data.scenes["Scene"].sequence_editor.active_strip
        # If review is enabled
        if active_strip and not active_strip.lock:
            # Change status
            row = layout.row(align=True)
            row.prop(scene, "media_status")

        layout.separator_spacer()

    def draw_preview(self, context):
        """Draw UI for Preview space"""
        scene = context.scene
        sequence_editor = scene.sequence_editor
        space_data = context.space_data
        layout = self.layout

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        # Don't dislay Main UI
        if scene is not bpy.data.scenes["Scene"]:
            layout.separator_spacer()
            ui_drawing.VIEW3D_HT_header.draw_stax_tools(self)
            layout.separator_spacer()
            return

        row = layout.row()
        row.operator("sequencer.refresh_all", text="", icon="FILE_REFRESH")
        if len(scene.tracks):
            # Select displayed track
            row.prop(
                scene,
                "current_displayed_track",
                text="",
                icon="SEQ_SEQUENCER",
                icon_only=True,
            )
            row.label(text=scene.tracks[space_data.display_channel - 1].name)
        else:
            row.prop(
                space_data,
                "display_channel",
                text="",
            )

        # Stop here if right panel of side by side
        sq_preview_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ]
        if len(sq_preview_areas) > 1 and context.area == sq_preview_areas[0]:
            return

        layout.separator_spacer()

        active_strip = bpy.data.scenes["Scene"].sequence_editor.active_strip
        # If review is enabled
        if active_strip and not active_strip.lock:

            # Active Tool
            # -----------
            from bl_ui.space_toolsystem_common import ToolSelectPanelHelper

            tool = ToolSelectPanelHelper.draw_active_tool_header(context, layout)

            # Hack to keep the current tool name
            context.screen["current_tool"] = tool.idname

            # If tool is annotate
            if tool.idname.startswith("builtin.annotate"):
                # Add empty keyframe for current layer
                layout.operator("scene.add_empty_gp_frame", icon="KEYFRAME_HLT")

        else:
            # If review disabled notify the user
            row = layout.row()
            row.alert = True
            if sequence_editor.active_strip:  # Review disabled
                row.label(text="You're not allowed to review this media", icon="ERROR")
            elif sequence_editor.sequences:  # No active media
                row.label(
                    text="Display a media to review it",
                    icon="ERROR",
                )
            row.alert = False

        layout.separator_spacer()

        # Show/Hide section
        row = layout.row()
        row.label(text="Show/Hide")
        # Toggle annotations
        if context.scene.all_annotations_displayed:
            row.operator(
                "sequencer.hide_sequences_annotations",
                text="",
                icon="GREASEPENCIL",
                depress=True,
            )
        else:
            row.operator(
                "sequencer.show_sequences_annotations",
                text="",
                icon="OUTLINER_DATA_GP_LAYER",
            )
        # Toggle text notes
        row.prop(space_data, "show_region_ui", icon="INFO", text="")


class SEQUENCER_PT_compare(bpy.types.Panel):
    """Tools to compare media"""

    bl_label = "Media Comparison Tools"
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "HEADER"
    bl_ui_units_x = 5

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        compare_properties = scene.compare_properties
        active_strip = scene.sequence_editor.active_strip

        # Current strip Fusion types and alpha
        if active_strip:
            layout.label(text="Compare Tools")

            layout.prop(scene.compare_properties, "compare_type", text="")

            # Wipe
            if compare_properties.compare_type == "WIPE":
                layout.prop(compare_properties, "wipe_fader", text="Split")
                layout.prop(compare_properties, "wipe_angle", text="Angle")
                layout.prop(compare_properties, "wipe_blur", text="Blur")

            # Alpha setting
            if compare_properties.compare_type == "CROSS":
                layout.prop(compare_properties, "opacity", text="Alpha")

        else:
            layout.label(text="No active strip")


class SEQUENCER_MT_context_menu(bpy.types.Menu):
    """Override of sequencer's default right click context menu"""

    bl_label = "Nothing right click. Ask developers to implement something."

    def draw(self, context):
        return


class DOPESHEET_MT_preview_range(bpy.types.Menu):
    bl_idname = "DOPESHEET_MT_preview_range"
    bl_label = "Set Preview Range"

    def draw(self, context):
        layout = self.layout
        layout.operator("anim.previewrange_set", icon="SELECT_SET", text="Box Select")
        layout.operator(
            "sequencer.set_preview_range_selected_sequences",
            icon="NLA",
            text="Selected",
        )


class PREVIEW_PT_stax_tools(bpy.types.Panel):
    """Add a custom tool panel in the Preview toolbar"""

    bl_label = " "
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "TOOLS"
    bl_parent_id = "SEQUENCER_PT_tools_active"
    bl_options = {"HIDE_HEADER"}

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.5

        active_strip = bpy.data.scenes["Scene"].sequence_editor.active_strip
        # If review is enabled
        if active_strip and not active_strip.lock:
            # Advanced drawing
            layout.operator("wm.advanced_drawing", icon="STROKE", text="")

            # Write text comment
            write_comment_icon = get_stax_icon("write_comment")
            layout.operator(
                "wm.write_comment", icon_value=write_comment_icon.icon_id, text=""
            )

            # Edit in external image editor
            if context.preferences.filepaths.image_editor:
                layout.operator(
                    "wm.edit_in_image_editor",
                    icon="SCREEN_BACK",
                    text="",
                )
            else:
                layout.operator(
                    "wm.select_external_image_editor",
                    icon="WORKSPACE",
                    text="",
                )

            layout.separator()

            # Clear drawings
            layout.operator("scene.clear_drawings", icon="TRASH", text="")


class PREVIEW_PT_pending_text_note(NOTES_PT_pending_text_note):
    """Pending Text note into Preview"""

    bl_space_type = "SEQUENCE_EDITOR"


class PREVIEW_PT_text_notes(NOTES_PT_text_notes):
    """Text notes into Preview"""

    bl_space_type = "SEQUENCE_EDITOR"


class PREVIEW_PT_media_info(bpy.types.Panel):
    """All current media info panel"""

    bl_label = "Media Info"
    bl_region_type = "UI"
    bl_category = "Info"
    bl_space_type = "SEQUENCE_EDITOR"

    def draw(self, context):
        pass


classes = (
    SEQUENCER_HT_header,
    SEQUENCER_HT_tool_header,
    SEQUENCER_MT_context_menu,
    DOPESHEET_MT_preview_range,
    SEQUENCER_PT_compare,
    PREVIEW_PT_stax_tools,
    PREVIEW_PT_pending_text_note,
    PREVIEW_PT_text_notes,
    PREVIEW_PT_media_info,
)


def register():
    # Set Custom UI
    # -------------
    for cls in classes:
        register_class(cls)


def unregister():
    # Clear Custom UI
    for cls in classes:
        unregister_class(cls)
