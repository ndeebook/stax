# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every ui class (panel, menu or header) relative to configuration
"""

import bpy


class CONFIG_PT_StaxConfiguration(bpy.types.Panel):
    """Panel for displaying Stax configuration"""

    bl_label = "Stax User Configuration"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "view_layer"

    @classmethod
    def poll(cls, context):
        return context.workspace is bpy.data.workspaces["Stax configuration"]

    def draw(self, context):
        layout = self.layout

        box = layout.box()
        box.prop(context.scene.user_preferences, "advanced_ui")
        box.prop(context.scene.user_preferences, "logging_auto_update")
        row = box.row(align=True)
        row.prop(context.scene.user_preferences, "autosave_delay")

        layout.separator()
        box = layout.box()
        box.label(text="Cache Management")
        row = box.row()
        row.prop(context.scene.user_preferences, "cache_directory")
        row.operator("scene.purge_cache", icon="TRASH", text="")
        box.prop(
            context.scene.user_preferences,
            "cache_expiration_delay",
            text="Expiration delay",
        )

        layout.separator()
        box = layout.box()
        box.prop(context.preferences.filepaths, "image_editor")

        layout.separator()

        # Linker
        layout.separator()
        box = layout.box()
        if context.scene.session_logged_in:
            box.label(
                text="Cannot change linker after logging. Please restart Stax and change this setting first.",
                icon="INFO",
            )
        else:
            box.prop(context.scene.user_preferences, "callback")

        layout.separator()

        row = layout.row(align=True)
        row.operator(
            "scene.change_stax_configuration",
            text="Save User Configuration",
            icon="PREFERENCES",
        ).save_configuration = True
        if context.window_manager.source_workspace:
            row.operator(
                "scene.change_stax_configuration",
                text="Quit without saving",
                icon="CANCEL",
            )


class CONFIG_PT_DevelopmentConfiguration(bpy.types.Panel):
    """Panel for development dedicated parameters"""

    bl_label = "Development"
    bl_parent_id = "CONFIG_PT_StaxConfiguration"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "view_layer"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout

        layout.prop(context.scene.user_preferences, "show_blender_ui")


classes = [CONFIG_PT_StaxConfiguration, CONFIG_PT_DevelopmentConfiguration]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
