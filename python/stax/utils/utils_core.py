# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
All functions to perform core actions shared by several operators
"""
from pathlib import Path
from datetime import datetime
import time
from typing import Tuple, Union
import base64

import bpy

from stax import python_dependencies


def install_dependency(
    dependency_name: str, target_path: Union[Path, str] = python_dependencies
):
    """Add a module to Blender's python through pip

    Installed in Stax application template's directory.

    :param dependency_name: Library name
    :param target_path: Path to install the dependency
    """
    from subprocess import run

    # Get Blender's Python binary
    pp = bpy.app.binary_path_python

    # Ensure pip (required for Linux only)
    run([pp, "-m", "ensurepip"])

    # Install module with pip
    run(
        [
            pp,
            "-m",
            "pip",
            "install",
            "--target",
            str(target_path),
            dependency_name,
        ]
    )


def datetime_from_utc_to_local(utc_datetime):
    """Convert utc datetime to local

    :param utc_datetime: Date time in UTC"""
    now_timestamp = time.time()
    offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(
        now_timestamp
    )
    return utc_datetime + offset


def get_audio_extensions() -> frozenset:
    """Get all file extensions that should be considered as audio
    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".wav",
            ".ogg",
            ".oga",
            ".mp3",
            ".mp2",
            ".ac3",
            ".aac",
            ".flac",
            ".wma",
            ".eac3",
            ".aif",
            ".aiff",
            ".m4a",
            ".mka",
        ]
    )


def get_video_extensions() -> frozenset:
    """Get all file extensions that should be considered as video
    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".avi",
            ".mpg",
            ".mpeg",
            ".dvd",
            ".vob",
            ".mp4",
            ".mov",
            ".dv",
            ".ogg",
            ".ogv",
            ".mkv",
            ".flv",
            ".webm",
        ]
    )


def get_image_extensions() -> frozenset:
    """Get all file extensions that should be considered as picture
    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".bmp",
            ".cin",
            ".dds",
            ".dpx",
            ".exr",
            ".hdr",
            ".j2c",
            ".jp2",
            ".jpeg",
            ".jpg",
            ".pdd",
            ".png",
            ".psb",
            ".psd",
            ".rgb",
            ".rgba",
            ".sgi",
            ".tga",
            ".tif",
            ".tiff",
            ".tx",
        ]
    )


def get_displayed_tracks() -> list:
    return [track.name for track in bpy.context.scene.tracks if track.displayed]


def get_selected_sequences() -> list:
    """Get selected sequences in a secure way

    context.selected_sequences is enough most of the time but this is useful if
    'Context' object has no attribute 'selected_sequences'.
    """
    context = bpy.context

    # Get selected sequences of only main scene in any case
    selected_sequences = [
        seq for seq in bpy.data.scenes["Scene"].sequence_editor.sequences if seq.select
    ]

    return selected_sequences


def get_collection_entity(collection: bpy.types.CollectionProperty, entity_name: str):
    """Return an entity from a collection. If the entity doesn't exists, it creates it.

    :param collection: Collection to get entity from
    :param entity_name: Name of the entity to get
    """
    entity = collection.get(entity_name)
    if not entity:
        if hasattr(collection, "add"):
            entity = collection.add()
            entity.name = entity_name
        elif hasattr(collection, "new"):
            entity = collection.new(entity_name)

    return entity


def load_attributes_to_property_group(
    attributes: Union[dict, list], property_group: bpy.types.PropertyGroup, silent=False
):
    """Load given user config in the given property group

    :param user_config: User config to load
    :param property_group: Property group to load config into
    :param silent: Doesn't prompt excepted errors.
    """
    # Sentinel for having an iterable
    if type(attributes) is dict:
        attributes = attributes.items()

    for k, v in attributes:
        try:
            if v is not None:  # Sentinel for default values
                attribute_type = type(getattr(property_group, k))
                setattr(property_group, k, attribute_type(v))
        except TypeError as err:
            if not silent:
                print(err)
                print(f"{k}: {v} can't be set for {property_group}")
        except AttributeError as err:
            if not silent:
                print(err)
                print(f"{k}: {v} can't be set for {property_group}")


def get_text_body(text: bpy.types.Text):
    """Convert Blender text object content to normal python string

    :param text: Text object to convert.
    """
    lines_body = [line.body + "\n" for line in text.lines]
    return "".join(lines_body).strip()


def report_message(
    report_type="INFO", message="Test Message from report_message function"
):
    """Call the wm.report_message operator to display a message into the Blender UI

    :param report_type: Type of report message
    :param message: Message content
    """
    # TODO: I'm not sure this function and the related operator should be kept. Maybe we only should pass a self.report
    #       as logger to every function we need to return user/dev feedback.
    # To keep blender naming convention and sets into loggers
    if isinstance(report_type, set) and hasattr(bpy.context, "view_layer"):
        report_type = list(report_type)[0]
        try:  # This try/except is to avoid a strange error message, TODO: if this function is kept, find out what's going on
            bpy.ops.wm.report_message(type=report_type, message=message)
        except RuntimeError:
            pass

    elif hasattr(bpy.context, "view_layer"):
        bpy.ops.wm.report_message(type=report_type, message=message)

    # If blender can't display message in the UI
    else:
        print(message)


def encode_password(password: str) -> str:
    """Hash a password for storing

    :param password: String to hash
    :return: Hashed string"""

    return base64.b64encode(password.encode("utf-8")).decode("utf-8")


def decode_password(password: str) -> str:
    """Verify a stored password against one provided by user

    :param password: String to decode
    :return: Decoded string
    """

    return base64.b64decode(password).decode("utf-8") if password else ""


def autosave():
    """Timer handler function for autosave"""
    bpy.ops.wm.save_mainfile()
    return bpy.context.scene.user_preferences.autosave_delay * 60


def get_context(
    screen_name: str, space_type: str, region_type="WINDOW", use_current_scene=False
):
    """Return specific context depending on the screen, space type and region if provided

    The aim of this function is to avoid to refactor all the operators overriding everytime the UI changes.
    A common usage is: get_context("Main", "PREVIEW") to get the Main viewer.
    This use requires to know a bit how Blender spaces are called and can be find via the console:
    ``bpy.data.screen["Main"].areas[0].spaces[0].type`` for example.

    :param screen_name: Screen name to get the spaces from
    :param space_type: The wanted space
    :param region_type: Specify the region type ("UI", "TOOLS", "HEADER", "TOOL_HEADER", "WINDOW"), defaults to "WINDOW"
    :param use_current_scene: Use the current active scene if specified, else use the default main scene
    """
    # Dedicated function, musn't be used elsewhere
    def get_area_attributes(
        space_type: str, area: bpy.types.Area, ui_type: str
    ) -> tuple:
        """Return the area attributes required by the context: area, space_data, region

        :param space_type: The wanted space
        :param area: The area containing the target space
        :param ui_type: The identifier to get the correct space
        :return: area, space_data, region
        """
        space_data = None
        region = None
        if space_type == getattr(area.spaces[0], ui_type):
            space_data = area.spaces[0]

            # Regions
            for r in area.regions:
                if r.type == region_type:
                    region = r
                    break

            return area, space_data, region
        else:
            return None, None, None

    # Get the screen
    screen = bpy.data.screens[screen_name]

    # Find area, space_data and region
    area = None
    space_data = None
    region = None
    for a in screen.areas:
        if hasattr(a.spaces[0], "view_type"):
            area, space_data, region = get_area_attributes(space_type, a, "view_type")
            if area and space_data and region:
                break
        elif hasattr(a.spaces[0], "mode"):
            area, space_data, region = get_area_attributes(space_type, a, "mode")
            if area and space_data and region:
                break
        elif hasattr(a, "type"):
            area, space_data, region = get_area_attributes(space_type, a, "type")
            if area and space_data and region:
                break

    # Sentinel warning
    if not screen or not area or not space_data or not region:
        print(f"Unknown space to get: {screen_name, space_type, region_type}")
        return

    context = {
        "area": area,
        "region": region,
        "scene": bpy.context.scene if use_current_scene else bpy.data.scenes["Scene"],
        "screen": screen,
        "space_data": space_data,
        "window": bpy.data.window_managers[0].windows[0],
        "window_manager": bpy.data.window_managers[0],
        "workspace": bpy.data.workspaces[screen_name],
    }

    return context


def get_text_contents(text_contents_list: list) -> Tuple[list, list]:
    """Extract comments and annotations from text contents list, as defined for text contents associated to sequences.

    Annotations have either a "frame_start" or a "frame_end" key.

    :param text_contents_list: Text contents list to extract comments and annotations from
    :return: Two lists: comments and annotations extracted from contents
    """
    annotations = [
        a
        for a in text_contents_list
        if a and (a.get("frame_start") is not None or a.get("frame_end") is not None)
    ]
    comments = [c for c in text_contents_list if c and c not in annotations]

    return comments, annotations
