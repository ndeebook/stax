# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""Run pytest tests."""

import importlib.util as import_util
import importlib
import os
from pathlib import Path
import sys

import blender_addon_tester as BAT


def main():
    app_template = Path(__file__).resolve().parent.parent.parent.parent
    blender_load_tests_script = app_template.joinpath(
        "python", "tests", "utils", "blender_load_pytest.py"
    )
    blender_rev = "2.83.12"

    result = BAT.test_blender_addon(
        blender_exec_path=os.environ["STAX_BL_PATH"],
        app_template_path=str(app_template),
        blender_revision=blender_rev,
        config={"blender_load_tests_script": str(blender_load_tests_script)},
        dir_to_ignore={".venv", ".git", "docs", "stax_dependencies"},
    )
    print("test result int ->", result)

    if result != 0:
        raise AssertionError("Tests failed")


main()
