import pytest
from addon_helper import get_version
import importlib
from pathlib import Path

import bpy

import opentimelineio as otio
import openreviewio as orio


def test_stax_import():
    """Stax module import"""

    assert importlib.import_module("bl_app_templates_user.stax")


def test_openreviewio_import():
    """OpenReviewIO module import"""

    assert importlib.import_module("openreviewio")


def test_opentimelineio_import():
    """OpenTimelineIO module import"""

    assert importlib.import_module("opentimelineio")


def test_load_timeline():
    """Timeline loading"""

    source_timeline_path = Path(__file__).parent.joinpath("data", "timeline_test.otio")
    source_timeline = otio.adapters.read_from_file(str(source_timeline_path))

    # Load timeline
    bpy.ops.sequencer.load_timeline(filepath=str(source_timeline_path))

    scene = bpy.context.scene
    sequence_editor = scene.sequence_editor

    # Tracks in sequencer matches the tracks in the source timeline
    assert len(source_timeline.tracks) == len(scene.tracks)
    assert {t.name for t in source_timeline.tracks} == {t.name for t in scene.tracks}

    # Sequences in sequencer matches the elements number
    elements = []
    for track in scene.tracks:
        for element in track.elements:
            elements.append(element)

    assert len(sequence_editor.sequences) == len(elements)

    # Fifth element has 3 versions and second has two
    assert len(elements[4].versions) == 3 and len(elements[1].versions) == 2

    # Review disabled media are locked
    correctly_locked = False
    for seq in sequence_editor.sequences:
        if not seq.lock == seq.get("review_enabled", True):
            correctly_locked = True
        else:
            correctly_locked = False
    assert correctly_locked


def test_link_reviews():
    """Link reviews"""

    orio_dir = Path(__file__).parent.joinpath("data", "OpenReviewIO")
    scene = bpy.context.scene
    sequence_editor = scene.sequence_editor

    # Link reviews
    bpy.ops.scene.link_reviews(directory=str(orio_dir))

    # Test the correct quantity of reviews have been imported
    assert len(scene.reviews) == 6

    # Test reviews linked to sequences
    for seq in sequence_editor.sequences:
        # The review exists
        if seq.get("review_name"):
            stax_review = scene.reviews.get(seq.get("review_name", ""))
            assert stax_review

            # It's the same as the get_orio_review function
            if stax_review:
                orio_review = orio.load_media_review(stax_review.path)
                assert orio_review.media == stax_review.get_orio_review().media
