.. _`load timeline`:

==========================
Load Timeline
==========================

Stax uses `OpenTimelineIO`_ to load timeline files. 

All the media from the editing ``.otio`` file will be loaded into Stax's timeline.

.. _OpenTimelineIO: https://opentimelineio.readthedocs.io/en/latest/

.. warning:: 
    It's currently impossible to correctly load a timeline which hasn't this `required structure`_.
    This will be changed in near future: https://gitlab.com/superprod/stax/-/issues/258.


How to
======
1. Click on ``Load Timeline``.

.. figure:: /_static/images/LoadTimeline_Button.png
          :alt: Load Timeline Button
          
          Load Timeline Button

2. Select the ``.otio`` file using the file browser.

Required structure
==================
Currently Stax requires a precise OTIO timeline structure.

.. graphviz:: otio_timeline_structure.dot

NB: This structure is converted into Stax's internal :ref:`stax timeline structure`.

Versions
========
This structure allows to set versions for a shot as tracks in a ``Stack``.  
In the sequencer, Stax will always use the last version by default. 
To display all the shot's versions, you can :ref:`toggle versions`.


Session configuration
=====================
The timeline can hold some metadata which will be used by Stax to configure the session.  
These metadata are associated to an element of the OTIO timeline:

.. code-block:: python

  element.metadata["param"] = "value"

Timeline
--------
*Settings handled by ``opentimelineio.schema.Timeline``*

Frame rate (FPS)
^^^^^^^^^^^^^^^^
Set the session frame rate.

``Timeline.metadata["frame_rate"] = float``

.. highlight:: python
.. code-block:: python

  timeline = otio.schema.Timeline()
  timeline.metadata["frame_rate"] = 25

Clip
----
*Settings handled by ``opentimelineio.schema.Clip``*

Sound track inclusion
^^^^^^^^^^^^^^^^^^^^^
Define if the current version should play the video's sound track (if the file is a video). Defaults to `false` if not set.

``Clip.metadata["include_sound"] = bool``

.. code-block:: python

  clip = otio.schema.Clip()
  clip.metadata["include_sound"] = True

Footage handles
^^^^^^^^^^^^^^^^
Define if the current version has frame handles at start and end: offset by a certain number of frames in the beginning and stripped at the end. Default is 0 frame handles, the whole media is used.

``Clip.source_range = TimeRange``

.. code-block:: python

  clip = otio.schema.Clip()
  clip.source_range = otio.opentime.range_from_start_end_time(
    otio.opentime.RationalTime(10, 25),
    otio.opentime.RationalTime(media_length - 10, 25)
  )


Review authorization
^^^^^^^^^^^^^^^^^^^^
Define if the current version is allowed to be reviewed.

``Clip.metadata["review_enabled"] = bool``

.. code-block:: python

  clip = otio.schema.Clip()
  clip.metadata["review_enabled"] = False

Preserve media ratio
^^^^^^^^^^^^^^^^^^^^
Make Stax to keep the media ratio in the viewport. Defaults to `false` if not set, Stax will fit and stretch the media to the scene's resolution (default Blender's behaviour).

``Clip.metadata["preserve_ratio"] = bool``

.. code-block:: python

  clip = otio.schema.Clip()
  clip.metadata["preserve_ratio"] = True

.. _`media-info`:

Media info displayed in Preview
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Display media info in Preview.

``Clip.metadata["stax_media_info"] = dict``

.. code-block:: python

  clip["stax_media_info"] = {
      "Shot": {
          "Assets": ["Camel", "Pinguin", "Berry"],
          "Due date": "01/02/03"
          },
      "Version": {
          "Artist": "Jean-Michel"
      }
  }

.. figure:: /_static/images/MediaInfo_Sidebar.png
   :alt: Media information in preview

   Media information in preview


Sample
======
:download:`Timeline sample </_static/samples/Sample.otio>`.

.. literalinclude:: /_static/samples/Sample.otio
   :language: json
   :emphasize-lines: 12,15,18,21,29,32-40,52,59,67,88-99
   :linenos:


Python
======
:attr:`~stax.ops.ops_timeline.LoadTimeline`


.. highlight:: python
.. code-block:: python

    bpy.ops.scene.load_timeline(filename="/path/to/timeline.otio")


Trouble shooting
================

Nothing appears in Stax's timeline
----------------------------------
#. Make sure your timeline correclty matches the `required structure`_.
#. Try to launch Stax using the :ref:`command line` and share the log on the `discord`_ to ask for assistance.

.. _discord: https://discord.gg/fceFNFH