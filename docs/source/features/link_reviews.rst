.. _`link reviews`:

==========================
Link Reviews
==========================

Regarding the reviews, Stax *links* them, it's not an import. This way an existing review is updated at every :ref:`publish <publish reviews>`.
This allows a continuous synchronization between all the users without any other third party software.

All Stax's reviews information are read and written using `OpenReviewIO <https://pypi.org/project/openreviewio/>`_, an open source standard, to ensure compatibility.

To :ref:`see <view>` existing reviews over media present in the :ref:`loaded timeline <load timeline>` you have to tell Stax to look into the directory where all the reviews are published.
Basically this directory contains several ``*.orio`` folders.


How to
======
1. Click on ``Link Reviews``.

.. figure:: /_static/images/LinkReviews_Button.png
        :alt: Link Reviews Button
        
        Link Reviews Button

2. Select the directory where the related reviews are stored.

.. figure:: /_static/images/LinkReviews_Browser.jpg
        :alt: Link Reviews Browser
        
        Link Reviews Browser

3. You should be able to :ref:`display <view>` the reviews.


Python
======
:attr:`~stax.ops.ops_session.SCENE_OT_link_reviews`


.. highlight:: python
.. code-block:: python

    bpy.ops.scene.link_reviews(director="/path/to/reviews_directory/")


Trouble shooting
================

My review isn't visible to the media
-------------------------------------

Stax matches the review to the corresponding media using the media file path.

If the expected review isn't visible when the media is displayed, you must check the media file path is the same of the ``media_path`` in the review file.

#. Go into the corresponding ``my_reviews_dir/my_media_name.mkv.orio`` review folder.
#. Open ``review.orio`` file. It's basically XML.
#. Make sure the value of ``media_path`` matches the media file path.