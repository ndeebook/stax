=====================
Properties
=====================

Core
========================
``classes_core.py``

.. automodule:: stax.properties.properties_core
   :members:
   :undoc-members:
   :show-inheritance:

Export
==========================
``classes_export.py``

.. automodule:: stax.properties.properties_export
   :members:
   :undoc-members:
   :show-inheritance:

Sequencer
==============================
``classes_sequencer.py``

.. automodule:: stax.properties.properties_sequencer
   :members:
   :undoc-members:
   :show-inheritance: