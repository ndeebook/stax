===============
Export timeline
===============

Stax allows you to export a “bout à bout” of your timeline.

.. video:: ../images/ExportTimeline.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

    Export Timeline

How to export
=============

-  Click on the **Export Mode** tab

-  Click **Add new render track** for each track you want to export

    -  Choose the track
    -  Choose the medias you want to export for that track

-  Configure your export

    -  Dimensions
    -  Output

-  Click on the **Export Render** button

-  Wait for the render to finish

-  That’s it

.. note::

    You can add up to 4 tracks in your export.
    The result will be a side by side render with tracks names.
    The export will only contains the chosen tracks.

.. warning::

    **You can’t cancel the export.**